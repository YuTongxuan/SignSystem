package SignSystem.SignSystem.utils;

import SignSystem.SignSystem.model.Employee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class EmployeeOperations {
    private Connection c = null;
    private Statement stmt = null;

    public void open() {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:SignSystemDB.db");
            c.setAutoCommit(true);
            stmt = c.createStatement();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    public void close() {
        try {
            stmt.close();
            c.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public Employee createEmployee(Employee employee) {
        try {
            open();
            String sql = "insert into employee (name) " +
                    "values ('"+ employee.getName() + "');";
            System.out.println(sql);
            stmt.executeUpdate(sql);
            String sqlQuery = "select * from employee where id = (select MAX(id) from employee)";
            ResultSet rs = stmt.executeQuery(sqlQuery);
            rs.next();
            employee.setId(rs.getInt("id"));
            rs.close();
            close();
            return employee;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteEmployee(int employeeId) {
        try {
            open();
            String sql = "delete from employee where id = " + employeeId + ";";
            stmt.executeUpdate(sql);
            close();
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }


    public List<Employee> getAllEmployee() {
        try {
            open();
            ResultSet rs = stmt.executeQuery( "select * from employee;" );
            List<Employee> retEmployeeArray = new ArrayList<Employee>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Employee employee = new Employee();
                employee.setId(id);
                employee.setName(name);
                retEmployeeArray.add(employee);
            }
            rs.close();
            close();
            return retEmployeeArray;
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

}
