package SignSystem.SignSystem.utils;

import SignSystem.SignSystem.model.Employee;
import SignSystem.SignSystem.model.Sign;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

public class SignOperations {
    private Connection c = null;
    private Statement stmt = null;

    public void open() {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:SignSystemDB.db");
            c.setAutoCommit(true);
            stmt = c.createStatement();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    public void close() {
        try {
            stmt.close();
            c.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public int getPageNum() {
        try{
            open();
            ResultSet rs = stmt.executeQuery("select count(*) as number from sign;");
            rs.next();
            int ret = rs.getInt("number");
            rs.close();
            close();
            int pageNum = ret / 5 + (ret % 5 == 0 ? 0 : 1);
            return pageNum;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return -1;
    }
    public List<Map<String, Object>> getPageSign(int page) {
        try {
            open();
            ResultSet rs = stmt.executeQuery(
                    "select name, type, time from sign left join employee on employee.id = sign.employeeId limit " +
                            5 * (page - 1) + ",5;");
            List<Map<String, Object>> retSignArray = new ArrayList<Map<String, Object>>();
            while (rs.next()) {
                String name = rs.getString("name");
                int type = rs.getInt("type");
                Date time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("time"));
                Map<String, Object> sign = new HashMap<String, Object>();
                sign.put("name", name);
                sign.put("type", type);
                sign.put("time", time);
                retSignArray.add(sign);
            }
            rs.close();
            close();
            return retSignArray;
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    public void createSign(Sign sign) {
        try {
            open();
            String sql = "insert into sign (employeeId, type, time) " +
                    "values("+ sign.getEmployeeId() + "," + sign.getType() + ", datetime('now'));";
            System.out.println(sql);
            stmt.executeUpdate(sql);
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Map<String, Object>> getSignToday(int employeeId) {
        try {
            open();
            ResultSet rs = stmt.executeQuery(
                    "select * from sign where strftime('%Y-%m-%d', time) = strftime('%Y-%m-%d', datetime('now')) and employeeId = " + employeeId + ";");
            List<Map<String, Object>> retSignArray = new ArrayList<Map<String, Object>>();
            while (rs.next()) {
                int employeeId1 = rs.getInt("employeeId");
                int type = rs.getInt("type");
                Date time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("time"));
                Map<String, Object> sign = new HashMap<String, Object>();
                sign.put("employeeId", employeeId1);
                sign.put("type", type);
                sign.put("time", time);
                retSignArray.add(sign);
            }
            rs.close();
            close();
            return retSignArray;
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }
}
