package SignSystem.SignSystem.controller;

import SignSystem.SignSystem.utils.EmployeeOperations;
import SignSystem.SignSystem.model.Employee;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class EmployeeController {
    @RequestMapping(value="/index")
    public String map1() {
        return "index";
    }

    @RequestMapping(value="/employee")
    public String map2() {
        return "employee";
    }

    @RequestMapping(value="/administrator")
    public String map3() {
        return "administrator";
    }


    EmployeeOperations employeeOperation = new EmployeeOperations();

    @RequestMapping(value="api/employee", method=RequestMethod.POST)
    @ResponseBody
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeOperation.createEmployee(employee);
    }

    @RequestMapping(value="api/employee/{employeeId}", method=RequestMethod.DELETE)
    @ResponseBody
    public void deleteEmployee(@PathVariable int employeeId) {
        employeeOperation.deleteEmployee(employeeId);
    }

    @RequestMapping(value="api/employee", method=RequestMethod.GET)
    @ResponseBody
    public List<Employee> getAllEmployee() {
        return employeeOperation.getAllEmployee();
    }


}





