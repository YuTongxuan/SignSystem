package SignSystem.SignSystem.controller;

import SignSystem.SignSystem.model.Sign;
import SignSystem.SignSystem.utils.SignOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
public class SignController {
    SignOperations signOperation = new SignOperations();

    @RequestMapping(value="api/sign/{page}", method=RequestMethod.GET)
    @ResponseBody
    public List<Map<String, Object>> getPageSign(@PathVariable int page) {
        return signOperation.getPageSign(page);
    }

    @RequestMapping(value="api/sign", method=RequestMethod.POST)
    @ResponseBody
    public void createSign(@RequestBody Sign sign) {
        signOperation.createSign(sign);
    }

    @RequestMapping(value="api/sign/pageNum", method=RequestMethod.GET)
    @ResponseBody
    public int getPageNum() {
        return signOperation.getPageNum();
    }

    @RequestMapping(value="api/sign/today/{employeeId}", method=RequestMethod.GET)
    @ResponseBody
    public List<Map<String, Object>> getSignToday(@PathVariable int employeeId) {
        return signOperation.getSignToday(employeeId);
    }

}
