idea里分包(文件夹），utils model controlloer

这次用的是全是vue形式的接口，不是jquery的，在前端代码中用v-on，在vue的methods里声明函数，在script里写function

# **业务逻辑:**

要从所有记录中调取当天记录，没签到签退的用户可以sign，签到过的用户只能签退，没签到过的用户不能签退，当天签到签退过的用户不能重复签；

在administrator页面中要显示总页面数和当前页面的5条sign，在Sign的controller中还要有求页数和getPageSign的函数

# Employee类

id, name

# Sign类

id(自身的自增长id, 数据库里不能为了方便避免歧义写成signId), employeeId, type(0, 1 表示签到签退）， time

**注意: 在administrator页面显示sign的时候是显示名字，但是为避免重名，sign中应存的是id，在数据库中把两个数据库关联起来：**

selet name, type, time from sign left join employee on employee.id = sign.employeeId 

前面的是table是调用的，后面的是附加的

# Controller

## EmployeeController

```
@RequestMapping(value="/index")
public String map1() {
    return "index";
}

@RequestMapping(value="/employee")
public String map2() {
    return "employee";
}

@RequestMapping(value="/administrator")
public String map3() {
    return "administrator";
}
```

+ create
+ delete
+ getAllEmployee

## SignController

时间可以选择用Date或者String类型

用Date的缺点：

1. 不同数据库不同，迁移成本，不适合开源软件

1. 前后端转换问题

String 的缺点：

要自己设计时间格式

+ getPageNum

+ createSign

+ getPageSign

+ getSignToday

# Operation

## EmployeeOperations

nothing special

## SignOperations

!!!注意：catch里不要放exit(0)了，一旦有错服务器会停，改成e.printStackTrace();

### getPageNum()

```
//求总页数
public int getPageNum() {
    try{
        open();
        //number = 数据总条数
        ResultSet rs = stmt.executeQuery("select count(*) as number from sign;");
        rs.next();
        int ret = rs.getInt("number");
        rs.close();
        close();
        //求页数，分是否是整页的情况
        int pageNum = ret / 5 + (ret % 5 == 0 ? 0 : 1);
        return pageNum;
    }
    catch(Exception e){
        e.printStackTrace();
    }
    return -1;
}
```

### getPageSign()

```
public List<Map<String, Object>> getPageSign(int page) {
    try {
        open();
        //取出的name, type, time没有括号！
        //不是employee.name
        //left join把后面的table附到前面的table
        //limit 0, 5 指的是从第0个开始取5个
        ResultSet rs = stmt.executeQuery(
                "select name, type, time from sign left join employee on employee.id = sign.employeeId limit " +
                        5 * (page - 1) + ",5;");
        //因为现在取出的不是sign对象，所以不能返回List<Sign>，返回List<Map<String, Object>>
        List<Map<String, Object>> retSignArray = new ArrayList<Map<String, Object>>();
        while (rs.next()) {
            String name = rs.getString("name");
            int type = rs.getInt("type");
            //记住下面这个！！
            Date time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("time"));
            //记住下面这个！！ new HashMap<String, Object>()对象
            Map<String, Object> sign = new HashMap<String, Object>();
            sign.put("name", name);
            sign.put("type", type);
            sign.put("time", time);
            retSignArray.add(sign);
        }
        rs.close();
        close();
        return retSignArray;
    } catch ( Exception e ) {
        e.printStackTrace();
    }
    return null;
}
```

### createSign()

```
public void createSign(Sign sign) {
    try {
        open();
        //values(..., date('now'))插入当前时间
        String sql = "insert into sign (employeeId, type, time) " +
                "values ('"+ sign.getEmployeeId() + "，" + sign.getType() + ", date('now'));";
        System.out.println(sql);
        stmt.executeUpdate(sql);
        String sqlQuery = "select * from sign where id = (select MAX(id) from sign)";
        ResultSet rs = stmt.executeQuery(sqlQuery);
        rs.next();
        sign.setId(rs.getInt("id"));
        rs.close();
        close();
    } catch (Exception e) {
        e.printStackTrace();
    }
}
```

### getSignToday()

```
public List<Map<String, Object>> getSignToday(int employeeId) {
    try {
        open();
        //取出当天记录
        ResultSet rs = stmt.executeQuery(
                "select * from sign where strftime('%Y-%m-%d', time) = " +
                        "strftime('%Y-%m-%d', datetime('now')) and employeeId = " + employeeId + ";");
        List<Map<String, Object>> retSignArray = new ArrayList<Map<String, Object>>();
        while (rs.next()) {
            String name = rs.getString("name");
            int type = rs.getInt("type");
            Date time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("time"));
            Map<String, Object> sign = new HashMap<String, Object>();
            sign.put("name", name);
            sign.put("type", type);
            sign.put("time", time);
            retSignArray.add(sign);
        }
        rs.close();
        close();
        return retSignArray;
    } catch ( Exception e ) {
        e.printStackTrace();
    }
    return null;
}
```

# 前端

## employee.html
```
function deleteEmployee(id) {
    console.log(`id = ${id}`);
    $.ajax({
        url: 'api/employee/' + id,
        contentType: 'application/json',
        type: 'DELETE',
        success: function() {
            let index = -1;
            //现在只知道要删掉的employee的id,要通过循环找到其在employees的位置index
            for(let i = 0; i < vueObject.employees.length; i++){
                if(id == vueObject.employees[i].id) {
                    index = i;
                    break;
                }
            }
            vueObject.employees.splice(index, 1);
        }
    });
}
```

create, get nothing special

## administrator.html
**注意:要分清哪些放在ajax里面，哪些放在外面，放在外面的是跟ajax并行的，要分清运行顺序和逻辑**

这里getPageNum和getPageSign不能分为两个独立的ajax，如果同时运行，同时open，可能一个还没运行完数据库就被close了，可能导致JVM crash

```
$.ajax({
    url: 'api/sign/pageNum',
    contentType: 'application/json',
    type: 'GET',
    success: function(pageNum) {
        vueObject.totalPage = pageNum;
        //getPageNum成功后再运行getPageSign
        setTableRows(1);
    }
});

function setTableRows(page) {
    $.ajax({
        url: 'api/sign/' + page,
        contentType: 'application/json',
        type: 'GET',
        success: function(signs) {
            signs.forEach(function(sign) {
                sign.time = new Date(sign.time);
            });
            vueObject.signs = signs;
        }
    });
}
```
数据库中的time是Date类型的，在传到服务器的过程中被字符串化，但前端不能识别传过来的是json中的字符串形式还是Date形式，要调用Date的构造函数把sign.time反序列化输出

可以在浏览器控制台中

var d = new Date();

d.to...String 各种String对应各种格式

toJson就是未变为Date类型前的样子



**写完服务器接口之后可以在浏览器控制台中写ajax调试,如：**

```
$.ajax({
    url: 'api/sign/pageNum',
    contentType: 'application/json',
    type: 'GET',
    success: function(pageNUm) {
        console.log(pageNUm);
    }
})
$.ajax({
    url: 'api/sign/1',
    contentType: 'application/json',
    type: 'GET',
    success: function(pageNUm) {
        console.log(pageNUm);
    }
})
```

形参无所谓